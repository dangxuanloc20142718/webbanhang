<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        // Create virtual DB
        $data = [];
        $ins_date = Carbon::now()->format('Y-m-d H:i:s');

        // List fake data for listing
        foreach (range(1, 40) as $index) {
            $data[] = [
                'full_name' => $faker->name,
                'email' => $faker->email,
                'address' => $faker->address,
                'phone' => '012345678',
                'password' => $faker->password,
                'ins_id' => 1,
                'ins_date' => $ins_date
            ];
        }

        // Insert DB
        DB::table('users')->insert($data);

    }
}
