<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class GoogleMapSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('googlemap')->truncate();
        // Create virtual DB
        $faker = Faker::create();
        $datas = [];
        for($i = 0; $i <10; $i++){
            $data = [
                'name' => $faker->name,
                'lating' => '',
                'address' => $faker->address
            ];
            $datas [] = $data;
        }
        DB::table('googlemap')->insert($datas);
    }
}
