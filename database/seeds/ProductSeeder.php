<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        DB::table('product')->truncate();
        $faker = Faker::create();
        $datas =[];
        for($i = 0; $i <10 ; $i++){
            $data['name'] = $faker->name;
            $data['quantity'] = rand(1,5);
            $data['price'] = rand(20000,70000);
            $data['insid'] = 1;
            $datas[] = $data;
        }
        DB::table('product')->insert($datas);
    }
}
