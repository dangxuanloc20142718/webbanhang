<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          DB::table('order')->truncate();
        // Create virtual DB
        $faker = Faker::create();
        $data = [
        	[ 'name' => "abcd",
        	'total' => 23000000,
        	'student_id' => 1
        	 ],

        	[  'name' => "mnpq",
        	'total' => 23000000, 
        	'student_id' => 1
       		]
       		,

        	[  'name' => "hjkl",
        	'total' => 23000000, 
        	'student_id' => 2
       		]
       		,

        	[  'name' => "zxcvbb",
        	'total' => 23000000, 
        	'student_id' => 2
       		]
        	
        ];
        DB::table('order')->insert($data);
    }
}
