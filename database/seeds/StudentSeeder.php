<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('student')->truncate();
        // Create virtual DB
        $faker = Faker::create();
        $data = [
        	'name' => $faker->name,
        	'age' => $faker->age,
        	'address' => $faker->address 
        ];
        DB::table('student')->insert($data);
    }
}
