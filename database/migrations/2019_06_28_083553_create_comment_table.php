<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comment', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_product')->length(10);
            $table->unsignedInteger('id_user')->length(10);
            $table->string('comment');
            $table->integer('ins_id');
            $table->dateTime('ins_date')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->integer('ups_id')->nullable();
            $table->dateTime('ups_date')->nullable();;
            //foreigkey
            $table->foreign('id_product')->references('id')->on('products');
            $table->foreign('id_user')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comment');
    }
}
