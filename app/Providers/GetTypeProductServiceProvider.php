<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\TypeProducts;
use Illuminate\Support\Facades\View;

class GetTypeProductServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(
            ['frontend.layout.header','frontend.category.index'],
            function($view){
                $listTypeProduct = TypeProducts::all();
                return $view->with('typeProduct',$listTypeProduct);
            }
        );
    }
}
