<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 6/27/2019
 * Time: 3:24 PM
 */

namespace App\Repositories;
use App\Models\Slides;

class BannerRepository extends BaseRepository
{
        public function __construct()
        {
            $this->_model = Slides::class;
        }
}
