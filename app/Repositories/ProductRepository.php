<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 6/27/2019
 * Time: 4:16 PM
 */

namespace App\Repositories;
use App\Models\Products;

class ProductRepository extends BaseRepository
{
    public function __construct()
    {
        $this->_model = Products::class;
    }
    public function getNewProduct(){
        $listNewProduct = $this->_model::where('new',1)->orderBy('ins_date','DESC')->paginate(4);
        return $listNewProduct;
    }
    public function getTopProduct(){
        $listTopProduct = $this->_model::Where('promotion_price','!=',0)->orderBy('ins_date','DESC')->paginate(4);
        return $listTopProduct;
    }
}
