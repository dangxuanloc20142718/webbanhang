<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 6/28/2019
 * Time: 10:15 AM
 */

namespace App\Repositories;
use App\Models\TypeProducts;
use App\Repositories\ProductRepository;

class CategoryRepository extends BaseRepository
{
    protected  $productRepository;
    public function __construct(ProductRepository $productRepository)
    {
        $this->_model = TypeProducts::class;
        $this->productRepository = $productRepository;
    }
    public function getNewProductOfCategory($id){
        $listProduct = $this->productRepository->getModel()->where('id_type',$id)->where('new',0)->paginate(3);
        return $listProduct;
    }
    public function getTopProductOfCategory($id){
        $listProduct = $this->productRepository->getModel()->where('id_type',$id)->where('promotion_price','!=',0)->paginate(3);
        return $listProduct;
    }
}
