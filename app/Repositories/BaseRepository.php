<?php

namespace App\Repositories;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use \Datetime;
use Illuminate\Support\Facades\DB;

class BaseRepository
{
    protected $_model;

    public function getModel()
    {
        return app()->make($this->_model);
    }

    public function all()
    {
        return $this->getModel()->all();
    }

    public function save($checkData, $data)
    {
        $data['upd_date'] = Carbon::now();
        $data['upd_id'] = Auth::guard('management')->user()->id;
                $data['ins_id'] = Auth::guard('management')->user()->id;
        return $this->getModel()->updateOrCreate($checkData,$data);
    }

    public function create($data)
    {
        $data['ins_date'] = Carbon::now();
        if(isShopRole()){
            $data['ins_id'] = Auth::guard('shop')->user()->id;
        }else{
            $data['ins_id'] = Auth::guard('management')->user()->id;
        }
        $object = new $this->_model($data);
        $object->save();
        return $object->id;
    }

    public function update($id, $data)
    {
        foreach ($data as $field => $value) {
            if (!in_array($field, $this->getModel()->getFillable())) {
                unset($data[$field]);
            }
        }
        $data['upd_date'] = Carbon::now();
        if(isShopRole()){
            $data['upd_id'] = Auth::guard('shop')->user()->id;
        }else{
            $data['upd_id'] = Auth::guard('management')->user()->id;
        }
        $object = $this->_model::find($id);
        foreach ($data as $field => $value) {
            if ($field != 'id' && in_array($field, $this->getModel()->getFillable())) {
                $object->$field = $value;
            }
        }
        $object->save();
        return true;
    }

    public function find($id)
    {
        return $this->getModel()->find($id);
    }

    public function softDelete($id)
    {
        $upd_date = Carbon::now();
        $upd_id = null;
        if(isShopRole()){
            $upd_id = Auth::guard('shop')->user()->id;
        }else{
            $upd_id = Auth::guard('management')->user()->id;
        }
        if (gettype($id) !== 'array'){
            $id = [$id];
        }
        return $this->getModel()::whereIn('id', $id)->update(['del_flag' => config('const.delete_on') . "",'upd_date' => $upd_date,'upd_id' => $upd_id]);
    }
}
