<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CategoryRepository;

class CategoryController extends Controller
{
    public $repository ;
    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index($id){
        $newProduct = $this->repository->getNewProductOfCategory($id);
        $topProduct = $this->repository->getTopProductOfCategory($id);
        $categoryName = $this->repository->find($id)->name;
        return view('frontend.category.index',compact('newProduct','topProduct','categoryName'));
    }
}
