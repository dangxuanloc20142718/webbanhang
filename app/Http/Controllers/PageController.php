<?php

namespace App\Http\Controllers;

use App\Repositories\BannerRepository;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public $repository;
    public $productRepositpory;
    public function __construct(BannerRepository $repository,ProductRepository $productRepository)
    {
        $this->repository = $repository;
        $this->productRepositpory = $productRepository;
    }

    public function index (){
        $listBanner = $this->repository->all();
        $newProduct = $this->productRepositpory->getNewProduct();
        $topProduct = $this->productRepositpory->getTopProduct();
        return view('frontend.homepage.index',compact('listBanner','newProduct','topProduct'));
    }
}
