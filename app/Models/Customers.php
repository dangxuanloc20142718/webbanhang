<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    protected $table = "customer";
    public function bills(){
        return $this->hasMany('app/Models/Customers');
    }
}
