<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeProducts extends Model
{
    protected $table = 'type_products';
    public function products(){
        return $this->hasMany('app/Models/products');
    }
}
