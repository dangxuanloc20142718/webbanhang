<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bills extends Model
{
    protected $table = 'bills';
    public function billDetail (){
        return $this->hasMany('app/Models/bill_detail');
    }
}
