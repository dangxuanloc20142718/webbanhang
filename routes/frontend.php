<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 6/26/2019
 * Time: 4:39 PM
 */
 Route::get('home','PageController@index')->name('page.home');
 Route::get('category/{id}','CategoryController@index')->name('page.category');
 Route::get('contact','ContactController@index')->name('page.contact');
 Route::get('about','AboutController@index')->name('page.about');
 Route::get('detail/{id}','DetailController@index')->name('page.detail');
