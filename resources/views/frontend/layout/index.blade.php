<!doctype html>
<html lang="en">
<head>
   @include('frontend.layout.meta')
</head>
<body>
@include('frontend.layout.header')
@yield('banner')
@yield('content')
@include('frontend.layout.footer')
</body>
</html>
