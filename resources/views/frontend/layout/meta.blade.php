<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Laravel </title>
<base href="{{asset('')}}">
<link href='http://fonts.googleapis.com/css?family=Dosis:300,400' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/dest/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/dest/vendors/colorbox/example3/colorbox.css">
<link rel="stylesheet" href="assets/dest/rs-plugin/css/settings.css">
<link rel="stylesheet" href="assets/dest/rs-plugin/css/responsive.css">
<link rel="stylesheet" title="style" href="assets/dest/css/style.css">
<link rel="stylesheet" href="assets/dest/css/animate.css">
<link rel="stylesheet" title="style" href="assets/dest/css/huong-style.css">
<style>
    .single-item-header {
        height: 265px;
    }
    .single-item-header img {
        height: 265px;
    }
    .single-item-price {
        margin: 4px auto;
    }
    .line-through {
        text-decoration: line-through;
    }
    .single-item {
        margin: 4px auto;
    }
</style>
