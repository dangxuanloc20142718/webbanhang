<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>jQuery UI Datepicker - Default functionality</title>
    <link rel="stylesheet" href="jquery-ui/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
    </script>
    <style>

        #box1 {
            max-width: 300px;
            max-height: 300px;
            overflow-y: visible;
        }
    </style>
</head>
<body>

<div class="contain">
    <div>giỏ hàng:{{ Session::has('cart') ? count(Session::get('cart')) : 0 }}</div>
    <div class="box" id="box1">
        <table>
            <tr>
                <th>Code</th>
                <th>Name</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>...</th>
                <th>..</th>
            </tr>
            @foreach($listProduct as $key => $pro)
                <tr>
                    <td>{{$key}}</td>
                    <td>{{$pro->name}}</td>
                    <td>{{$pro->quantity}}</td>
                    <td>{{$pro->price}}</td>
                    <td><input type="text"></td>
                    <td> <button tyle="button" onclick="addProduct('{{route('addCart')}}',this,{{$key}})">addProduct</button></td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
</body>
</html>
<script>
        function addProduct(path,e,id) {
             var quantity = $(e).closest('tr').find('input[type=text]').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: path,
                type: 'get',
                data: {
                        sl : quantity,
                        id : id,

                }
            });

        }
</script>
