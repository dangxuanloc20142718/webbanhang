<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Page Description">
    <meta name="author" content="DELL">
    <title>Page Title</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
            margin: 0px auto;
        }
        .container {
            width: 100%;
        }
        .navigation1 {
            width: 100%;
            background: #373c43;
            color: white;
        }
        .navigation1-left {
            width: 50%;
            float: left;
        }
        .navigation1 .navigation1-left .menu1 {
            list-style-type: none;
            line-height: 40px;
        }
        .navigation1 .navigation1-left .menu1 li {
            text-align: center;
            display: inline-block;
            margin-left: -5px;
        }
        .navigation1 .navigation1-left .menu1 a{
            color: white;
            text-decoration: none;
            display: block;
            width: 90px;
        }
        .navigation1 .navigation1-left .menu1 li:hover {
            background: #636b6f;
            color: #333;
        }
        .navigation1-right {
           float: right;
            margin-top:10px;
        }
        .navigation1 .navigation1-right .menu2{
            list-style-type: none;
        }
        .navigation1 .navigation1-right .menu2 a{
            text-decoration: none;
            display: block;
            width: 30px;
        }
        .navigation1 .navigation1-right .menu2 li {
            text-align: center;
            display: inline-block;
            margin-left: -5px;
        }
        .navigation1 .navigation1-right .menu2 li:hover {
            background: #636b6f;
            color: #333;
        }
        .banner {
            width: 60%;
            text-align: left;
            margin-top: 20px;
        }
        .banner img {
            width: 250px;
        }
        .navigation-content {
            width: 60%;
            overflow: auto;
        }
        .clear {
            clear: both;
        }
        .navigation2 {
            width: 100%;
            background-color: #f0f0f0;
            margin-top: 20px;
        }
        .navigation2-content {
            width: 60%;
        }
        .navigation2 .navigation2-content .navigation2-left{
            display: inline-block;
        }
        .navigation2 .navigation2-content .navigation2-right{
            display: inline-block;
            margin-left: 125px;
        }
        .navigation2 .navigation2-content .navigation2-left .menu3 {
            list-style-type: none;
            line-height: 40px;
        }
        .navigation2 .navigation2-content .navigation2-left .menu3   li{
            display: inline-block;
            text-align: center;
            margin-left: -5px;
            width: 120px;
            position: relative;
            height: 40px;
        }
        .navigation2 .navigation2-content .navigation2-left .menu3  a{
            display: block;
            text-decoration: none;
        }
        .navigation2 .navigation2-content .navigation2-left .menu3  li:hover{
            background: white;
            color: #333;
        }
        .navigation2-content .navigation2-right .menu4 {
            list-style-type: none;
            line-height: 40px;
            background-color: white;
            margin-left: 13px;
        }
        .navigation2-content .navigation2-right .menu4 li {
            width: 50px;
            text-align: center;
        }
        .sub-menu3 {
            display: none;
            position: absolute;
            list-style-type: none;
            text-align: left;
            width: 200px;
        }
        .menu3 li:hover .sub-menu3 {
            background-color: white;
            display: block;
        }
        .sub-menu3 {
            list-style-type: none;
        }
        .sub-menu3 a{
            color: black;
            width: 200px;
            text-align: left;
            padding-left: 30px;
        }
        .sub-menu3 a:hover  {
            color: orange;
            background-color: #6cb2eb;
        }
        .breadcump {
            width: 60%;
            height: 30px;
            background-color: white;
            padding-left: 20px;
            border: 1px solid #dfdfdf;
        }
        .content-body{
            padding-top: 10px;
            width: 100%;
            background-color: #f0f0f0;
            margin-top: 10px;
        }
        .breadcump li {
            list-style-type: none;
            display: inline-block;
            padding-top: 8px;
        }
        .content-body .body {
            width: 60%;
            margin-top: 10px;
            margin-bottom: 30px;
            overflow: auto;
        }
        .body-left {
            width: 68%;
            float: left;
        }
        .body-right {
            float: right;
            width:30%;
        }
        .content1 {
            width: 100%;
            text-align: left;
            height: 180px;
            background-color: white;
            padding: 10px 15px;
        }
        .content11 {
            width: 100%;
            text-align: left;
            height: 230px;
            background-color: white;
            padding: 20px 15px;
            margin-top: 10px;
        }
        .content1 div {
            color:#c6c6c6;
            border-bottom: 1px solid #c6c6c6;
            margin-top: 10px;
        }
        .content1 p {
            margin-top: 25px;
        }
        .content2 {
            width: 100%;
            background-color: white;
        }
        .content2 table {
            width: 100%;
        }
        .content2 td {
            border-top:1px solid #c6c6c6;
            width: 100%;
        }
        .content2 table tr {
            line-height: 40px;
        }
        .content2 table td {
            padding-left: 10px;
        }

        .content21 {
            width: 100%;
            background-color: white;
            margin-top: 10px;
            padding-bottom: 20px;
        }
        .content21 table {
            width: 100%;
        }
        .content21 table, .content21 th, .content21 td {
            width: 100%;
        }
        .content21 table tr {
            line-height: 40px;
        }
        .content21 table td {
            padding-left: 10px;
        }
        .content21 table td div {
            background-color: red;
            width: 90%;
            padding-left: 10px;
        }
        .content21 table td div a{
            text-decoration: none;
            color: white;
            font-weight: bold;
        }
        /* Slideshow container */
        .slideshow-container {
            max-width: 600px;
            position: relative;
            margin: auto;
        }

        /* Hide the images by default */
        .mySlides {
            display: none;
        }

        /* Next & previous buttons */
        .prev, .next {
            cursor: pointer;
            position: absolute;
            top: 50%;
            width: auto;
            margin-top: -22px;
            padding: 16px;
            color: white;
            font-weight: bold;
            font-size: 18px;
            transition: 0.6s ease;
            border-radius: 0 3px 3px 0;
            user-select: none;
        }

        /* Position the "next button" to the right */
        .next {
            right: 0;
            border-radius: 3px 0 0 3px;
        }

        /* On hover, add a black background color with a little bit see-through */
        .prev:hover, .next:hover {
            background-color: rgba(0,0,0,0.8);
        }

        /* Caption text */
        .text {
            color: #f2f2f2;
            font-size: 15px;
            padding: 8px 12px;
            position: absolute;
            bottom: 8px;
            width: 100%;
            text-align: center;
        }

        /* Number text (1/3 etc) */
        .numbertext {
            color: #f2f2f2;
            font-size: 12px;
            padding: 8px 12px;
            position: absolute;
            top: 0;
        }

        /* The dots/bullets/indicators */
        .dot {
            cursor: pointer;
            height: 15px;
            width: 15px;
            margin: 0 2px;
            background-color: #bbb;
            border-radius: 50%;
            display: inline-block;
            transition: background-color 0.6s ease;
        }

        .active, .dot:hover {
            background-color: #717171;
        }

        /* Fading animation */
        .fade {
            -webkit-animation-name: fade;
            -webkit-animation-duration: 1.5s;
            animation-name: fade;
            animation-duration: 1.5s;
        }

        .content2 td a {
            text-decoration: none;
            color: black;
        }
        .content12 {
            width: 100%;
            height: 300px;
            background-color: white;
            margin-top: 10px;
        }
        @media all and (min-width: 481px) and (max-width: 768px) {
            .navigation-content menu1 a {
            }
            .body-left{
                float: left;
                width: 100%;
            }
            .body-right {
                float: left;
                width: 100%;
            }
            .content11 {
                display: none;
            }
            .content12 {
                display: none;
            }
        }
        .content22 {
            width: 100%;
            margin-top: 10px;
            background-color: white;
        }
        .sub-content22 {
            width: 100%;
            overflow: auto;
            border: 1px solid #eaeaea;
        }
        .sub-content22-left {
            float: left;
            width: 30%;
        }
        .sub-content22-right {
            float: right;
            width: 70%;
            padding-left: 10px;
        }
        .sub-content22:first-child {
            padding: 10px 0;
            text-align: center;
        }
        .sub-content22-right a {
            color: black;
            text-decoration: none;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="navigation1">
        <div class="navigation-content">
            <div class="navigation1-left">
                <ul class="menu1">
                    <li><a href="">Giới thiệu</a></li>
                    <li><a href="">Bản quyền</a></li>
                    <li><a href="">Liên hệ</a></li>
                </ul>
            </div>
            <div class="navigation1-right">
                <ul class="menu2">
                    <li><a href=""><i class="fa fa-facebook-f fa-1x" style="color:white"></i></a></li>
                    <li><a href=""><i class="fa fa-google-plus fa-1x" style="color:white"></i></a></li>
                    <li><a href=""><i class="fa fa-youtube-square fa-1x" style="color:white"></i></a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="clear"></div>

    <div class="banner">
        <img src="img/logo.png">
    </div>
    <div class="navigation2">
        <div class="navigation2-content">
            <div class="navigation2-left">
                <ul class="menu3">
                    <li><a>WEBMASTER</a>
                        <ul class="sub-menu3">
                            <li><a href="#">Mã giảm giá</a></li>
                            <li><a href="#">Hosting VPS </a></li>
                            <li><a href="#">Thủ thuật hosting</a></li>
                        </ul>
                    </li>
                    <li><a>LẬP TRÌNH</a>
                        <ul class="sub-menu3">
                            <li><a href="#">Học php</a></li>
                            <li><a href="#">Học laravel</a></li>
                            <li><a href="#">Học phacol</a></li>
                        </ul>
                    </li>
                    <li><a>DATABASE</a>
                        <ul class="sub-menu3">
                            <li><a href="#">Học MYSQL</a></li>
                            <li><a href="#">Hoc ORACLE </a></li>
                            <li><a href="#">Hoc PorSQL</a></li>
                        </ul>
                    </li>
                    <li><a>KHÓA HỌC</a>
                        <ul class="sub-menu3">
                            <li><a href="#">Học Backend</a></li>
                            <li><a href="#">Hoc Frontend </a></li>
                            <li><a href="#">Học Mobile</a></li>
                        </ul>
                    </li>
                    <li><a>CÔNG NGHỆ</a>
                        <ul class="sub-menu3">
                            <li><a href="#">Điện thoại</a></li>
                            <li><a href="#">Phụ kiện</a></li>
                            <li><a href="#">Thiết bị vp</a></li>
                        </ul>
                    </li>
                    <li><a>TÀI NGUYÊN</a>
                        <ul class="sub-menu3">
                            <li><a href="#">Sách</a></li>
                            <li><a href="#">Phần mềm</a></li>
                            <li><a href="#">Tài liệu</a></li>
                        </ul>
                    </li>
                    <li><a>VIỆC LÀM</a>
                        <ul class="sub-menu3">
                            <li><a href="#">Web developer</a></li>
                            <li><a href="#">Web designer</a></li>
                            <li><a href="#">Tester</a></li>
                        </ul>
                    </li>
                    <li><a>TOOLS</a>
                        <ul class="sub-menu3">
                            <li><a href="#">Mã giảm giá</a></li>
                            <li><a href="#">Hosting VPS </a></li>
                            <li><a href="#">Thủ thuật hosting</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="navigation2-right">
                <ul class="menu4">
                    <li>
                        <i class="fa fa-search"></i>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="clear"></div>
    <div class="content-body">
        <div class="breadcump">
            <ul>
                <li>Trang chủ</li>  <li> <i class="fa fa-chevron-right"></i><i class="fa fa-chevron-right"></i> Web master</li>
            </ul>
        </div>
        <div class="body">
            <div class="body-left">
                <div class="content1">
                    <h1>Webmaster</h1>
                    <div>Đăng bởi: Administrator | Vào ngày: 04-10-2017</div>
                    <p>Tổng hợp những bài viết chia sẻ về quản trị website, thủ thuật mua VPS và Hosting. Tại đây bạn sẽ được hướng dẫn tất cả những thứ cần thiết để có thể quản trị được website.</p>
                </div>
                <div class="content11">
                    <div class="slideshow-container">

                        <!-- Full-width images with number and caption text -->
                        <div class="mySlides fade">
                            <div class="numbertext">1 / 3</div>
                            <img src="img/banner.jpg" style="width:600px;height: 150px">
                            <div class="text">Caption Text</div>
                        </div>

                        <div class="mySlides fade">
                            <div class="numbertext">2 / 3</div>
                            <img src="img/change.jpg" style="width:600px;height: 150px">
                            <div class="text">Caption Two</div>
                        </div>

                        <div class="mySlides fade">
                            <div class="numbertext">3 / 3</div>
                            <img src="img/appedicate.jpg" style="width:600px;height: 150px">
                            <div class="text">Caption Three</div>
                        </div>

                        <!-- Next and previous buttons -->
                        <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                        <a class="next" onclick="plusSlides(1)">&#10095;</a>
                    </div>
                    <br>

                    <!-- The dots/circles -->
                    <div style="text-align:center">
                        <span class="dot" onclick="currentSlide(1)"></span>
                        <span class="dot" onclick="currentSlide(2)"></span>
                        <span class="dot" onclick="currentSlide(3)"></span>
                    </div>
                </div>
                <div class="content12">
                    <div class="sub1-content12">
                        <div>
                            <img src="">
                        </div>
                        <div></div>
                    </div>
                    <div class="sub1-content12">
                        <div>
                            <img src="">
                        </div>
                        <div></div>
                    </div>
                    <div class="sub1-content12">
                        <div>
                            <img src="">
                        </div>
                        <div></div>
                    </div>
                </div>
            </div>
            <div class="body-right">
                <div class="content2">
                   <table>
                       <tr>
                           <th>CHUYÊN MỤC</th>
                       </tr>
                       <tr>
                           <td><i class="fa fa-star"></i> <a href="#">Hosting/VPS miễn phí</a></td>
                       </tr>
                       <tr>
                           <td><i class="fa fa-star"></i> <a href="#">Hosting/VPS giá rẻ</a></td>
                       </tr>
                       <tr>
                           <td><i class="fa fa-star"></i> <a href="#">Thủ thuật Hosting</a></td>
                       </tr>
                       <tr>
                           <td><i class="fa fa-star"></i> <a href="#" >Thủ thuật VPS</a></td>
                       </tr>
                       <tr>
                           <td> <i class="fa fa-star"></i> <a href="#">Thủ thuật VPS</a></td>
                       </tr>
                       <tr>
                           <td> <i class="fa fa-star"></i> <a href="#">Thủ thuật WordPress</a></td>
                       </tr>
                   </table>
                </div>
                <div class="content21">
                    <table>
                        <tr>
                            <th>MÃ GIẢM GIÁ</th>
                        </tr>
                        <tr>
                            <td><div><a href="">DOMAIN</a></div></td>
                        </tr>
                        <tr>
                            <td><div><a>HOSTING</a></div></td>
                        </tr>
                        <tr>
                            <td><div><a>VPS-SERVER</a></div></td>
                        </tr>
                        <tr>
                            <td><div><a>CHỨNG CHỈ SSL</a></div></td>
                        </tr>
                        <tr>
                            <td><div><a>PHẦN MỀM</a></div></td>
                        </tr>
                        <tr>
                            <td><div><a>TIN TỨC</a></div></td>
                        </tr>
                    </table>
                </div>
                <div class="content22">
                    <div class="sub-content22">
                        <h3>HỌC ONLINE</h3>
                    </div>
                    <div class="sub-content22">
                        <div class="sub-content22-left">
                            <img src="img/ui.jpg">
                        </div>
                        <div class="sub-content22-right">
                            <a href="#">Học thiết kế UX/UI Web/App bằng Adobe Photoshop, Adobe Muse và Adobe Experience design CC 2017</a>
                        </div>
                    </div>
                    <div class="sub-content22">
                        <div class="sub-content22-left">
                            <img src="img/android.gif">
                        </div>
                        <div class="sub-content22-right">
                            <a href="">Tự học lập trình Android cho người mới bắt đầu</a>
                        </div>
                    </div>
                    <div class="sub-content22">
                        <div class="sub-content22-left">
                            <img src="img/php.gif">
                        </div>
                        <div class="sub-content22-right">
                            <a href="">Giới thiệu khóa học Laravel cho người mới bắt đầu</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer"></div>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script>
    var slideIndex = 1;
    showSlides(slideIndex);

    // Next/previous controls
    function plusSlides(n) {
        showSlides(slideIndex += n);
    }

    // Thumbnail image controls
    function currentSlide(n) {
        showSlides(slideIndex = n);
    }

    function showSlides(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("dot");
        if (n > slides.length) {slideIndex = 1}
        if (n < 1) {slideIndex = slides.length}
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex-1].style.display = "block";
        dots[slideIndex-1].className += " active";
    }
</script>
</body>
</html>
