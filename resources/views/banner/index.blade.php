@extends('layout.index)
@section('content')
    <div id="PageContent">
        <h2><br/><i class="fas fa-image"></i> バナー一覧<span class="btn register btnMedium" style="margin-top:-40px;"><button type="button" onclick="location.href='banner_add.html'" tabindex="14"><i class="fas fa-plus"></i> バナー登録</button></span></h2>
        <table class="OrderForm responsive tdcenter Bordertd table-hover">
            <tr>
                <th scope="col" style="width: 100px;">ID</th>
                <th scope="col">バナー画像</th>
                <th scope="col">公開期間</th>
                <th scope="col">ステータス</th>
                <th scope="col">リンクURL</th>
                <th scope="col" style="width: 110px;">編集</th>
                <th scope="col" style="width: 110px;">削除</th>
            </tr>
            <tr>
                <td>1</td>
                <td style="text-align:left;"><img src="./img/common/1.jpg" width="200"></td>
                <td style="text-align:left;">2019/01/01〜2019/12/31</td>
                <td style="text-align:left;">有効</td>
                <td style="text-align:left;"><a href="http://sample.co.jp" target="_blank">http://sample.co.jp</a></td>
                <td><p class="btnmini Edit">
                        <a href="banner_edit.html" class="TextC">編集</a>
                    </p></td>
                <td>
                    <p class="btnmini">
                        <button href="#" type="button"  value="">削除
                        </button>
                    </p>
                </td>
            </tr>

        </table>
    </div>
@endsection
