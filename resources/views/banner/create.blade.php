<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>行事商品ネット予約サービス管理画面</title>
    <link href="css/jquery-ui.min.css" rel="stylesheet">
    <link href="css/responsive-tables.css" rel="stylesheet">
    <link href="css/slicknav.css" rel="stylesheet">
    <link href="css/common.css" rel="stylesheet">
    <link href="css/pagestyle.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/mobile_style.css">
    <script src="js/jquery-1.11.3.min.js"></script>
    <link rel="shortcut icon" href="img/common/favicon.ico" type="image/vnd.microsoft.icon">
    <link rel="icon" href="img/common/favicon.ico" type="image/vnd.microsoft.icon">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <!--[if lt IE 9]>
    <script src="js/html5shiv-printshiv.js"></script>
    <script src="js/selectivizr.js"></script>
    <![endif]-->

    <!--[if IE 8]>
    <html class="ie ie8 lt-ie9">
    <![endif]-->
    <!--[if IE 7]>
    <html class="ie ie7 lt-ie9 lt-ie8">
    <![endif]-->
</head>
<body>
<a id="pagetop"></a>

<div id="header">
    <div id="headerIn">
        <p id="logo"><img src="img/common/logo-pc.png" alt="logo" class="switch"></p>
        <p class="StoreName"><a href="#"> テスト 太郎 </a></p>
        <p class="LogOut"><a href="login.html">ログアウト</a></p>
        <div id="headerNav">
            <ul>
                <li class="nav01 menu__multi"><a href="#">予約管理</a>
                    <ul class="menu__second-level">
                        <li><a href="order_list.html">予約一覧</a></li>
                    </ul>
                </li>
                <li class="nav05 menu__multi"><a href="#">取込データ確認</a>
                    <ul class="menu__second-level">
                        <li><a href="event_list.html">行事コード</a></li>
                        <li><a href="category_list.html">カテゴリーコード</a></li>
                        <li><a href="event_category_list.html">行事カテゴリー</a></li>
                        <li><a href="event_view_list.html">行事表示</a></li>
                        <li><a href="event_item_list.html">行事商品表示</a></li>
                        <li><a href="item.html">商品</a></li>
                        <li><a href="item_name.html">商品名称</a></li>
                        <li><a href="item_price.html">商品価格</a></li>
                        <li><a href="item_point.html">ボーナスポイント</a></li>
                        <li><a href="item_shop_count.html">店別計画</a></li>
                        <li><a href="item_limited.html">商品別限定</a></li>
                    </ul>
                </li>
                <li class="nav03 menu__multi"><a href="#">商品管理</a>
                    <ul class="menu__second-level">
                        <li><a href="item_list.html">商品一覧</a></li>
                    </ul>
                </li>
                <li class="nav04 menu__multi"><a href="#">店舗管理</a>
                    <ul class="menu__second-level">
                        <li><a href="shop_list.html">店舗一覧</a></li>
                        <li><a href="shop_add.html">店舗登録</a></li>
                    </ul>
                </li>
                <li class="nav02 menu__multi"><a href="#">お客様管理</a>
                    <ul class="menu__second-level">
                        <li><a href="user_list.html">お客様一覧</a></li>
                    </ul>
                </li>
                <li class="nav07 menu__multi"><a href="#">お知らせ管理</a>
                    <ul class="menu__second-level">
                        <li><a href="information_list.html">お知らせ一覧</a></li>
                        <li><a href="information_add.html">お知らせ登録</a></li>
                    </ul>
                </li>
                <li class="nav06 menu__multi"><a href="#" class="active">各種設定</a>
                    <ul class="menu__second-level">
                        <li><a href="banner_list.html">バナー一覧</a></li>
                        <li><a href="banner_add.html">バナー登録</a></li>
                        <li><a href="account_list.html">アカウント一覧</a></li>
                        <li><a href="account_add.html">アカウント登録</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <!--headerNav -->
    </div>
    <!--headerIn -->
</div>
<!--header -->

<div id="Contentwrap">
    <div id="PageContent">
        <h2><br/><i class="fas fa-image"></i> バナー一覧<span class="btn register btnMedium" style="margin-top:-40px;"><button type="button" onclick="location.href='banner_add.html'" tabindex="14"><i class="fas fa-plus"></i> バナー登録</button></p></span></h2>
        <table class="OrderForm responsive tdcenter Bordertd table-hover">
            <tr>
                <th scope="col" style="width: 100px;">ID</th>
                <th scope="col">バナー画像</th>
                <th scope="col">公開期間</th>
                <th scope="col">ステータス</th>
                <th scope="col">リンクURL</th>
                <th scope="col" style="width: 110px;">編集</th>
                <th scope="col" style="width: 110px;">削除</th>
            </tr>
            <tr>
                <td>1</td>
                <td style="text-align:left;"><img src="./img/common/1.jpg" width="200"></td>
                <td style="text-align:left;">2019/01/01〜2019/12/31</td>
                <td style="text-align:left;">有効</td>
                <td style="text-align:left;"><a href="http://sample.co.jp" target="_blank">http://sample.co.jp</a></td>
                <td><p class="btnmini Edit">
                        <a href="banner_edit.html" class="TextC">編集</a>
                    </p></td>
                <td>
                    <p class="btnmini">
                        <button href="#" type="button" onclick="MobileController.showPopupDeleteCategory(this)" value="">削除
                        </button>
                    </p>
                </td>
            </tr>

        </table>
    </div>
    <!--PageContent -->

</div>
<!--Contentwrap -->

<script src="js/jquery-ui.min.js"></script>
<script src="js/responsive-tables.js"></script>
<script type="text/javascript" src="js/jquery-ui-i18n.min.js"></script>
<script src="js/jquery.slicknav.js"></script>
<script src="js/common.js"></script>
<script type="text/javascript">
    (function ($) {
        $(document).ready(function () {
            $('#headerNav ul').slicknav();
        });
    })(jQuery);


</script>


</body>
</html>
