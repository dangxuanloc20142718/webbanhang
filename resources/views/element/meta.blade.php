<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>行事商品ネット予約サービス管理画面</title>
<link href="css/jquery-ui.min.css" rel="stylesheet">
<link href="css/responsive-tables.css" rel="stylesheet">
<link href="css/slicknav.css" rel="stylesheet">
<link href="css/common.css" rel="stylesheet">
<link href="css/pagestyle.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/mobile_style.css">
<script src="js/jquery-1.11.3.min.js"></script>
<link rel="shortcut icon" href="img/common/favicon.ico" type="image/vnd.microsoft.icon">
<link rel="icon" href="img/common/favicon.ico" type="image/vnd.microsoft.icon">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
