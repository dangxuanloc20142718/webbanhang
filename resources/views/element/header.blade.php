<div id="headerIn">
    <p id="logo"><img src="img/common/logo-pc.png" alt="logo" class="switch"></p>
    <p class="StoreName"><a href="#"> テスト 太郎 </a></p>
    <p class="LogOut"><a href="login.html">ログアウト</a></p>
    <div id="headerNav">
        <ul>
            <li class="nav01 menu__multi"><a href="#">予約管理</a>
                <ul class="menu__second-level">
                    <li><a href="order_list.html">予約一覧</a></li>
                </ul>
            </li>
            <li class="nav05 menu__multi"><a href="#">取込データ確認</a>
                <ul class="menu__second-level">
                    <li><a href="event_list.html">行事コード</a></li>
                    <li><a href="category_list.html">カテゴリーコード</a></li>
                    <li><a href="event_category_list.html">行事カテゴリー</a></li>
                    <li><a href="event_view_list.html">行事表示</a></li>
                    <li><a href="event_item_list.html">行事商品表示</a></li>
                    <li><a href="item.html">商品</a></li>
                    <li><a href="item_name.html">商品名称</a></li>
                    <li><a href="item_price.html">商品価格</a></li>
                    <li><a href="item_point.html">ボーナスポイント</a></li>
                    <li><a href="item_shop_count.html">店別計画</a></li>
                    <li><a href="item_limited.html">商品別限定</a></li>
                </ul>
            </li>
            <li class="nav03 menu__multi"><a href="#">商品管理</a>
                <ul class="menu__second-level">
                    <li><a href="item_list.html">商品一覧</a></li>
                </ul>
            </li>
            <li class="nav04 menu__multi"><a href="#">店舗管理</a>
                <ul class="menu__second-level">
                    <li><a href="shop_list.html">店舗一覧</a></li>
                    <li><a href="shop_add.html">店舗登録</a></li>
                </ul>
            </li>
            <li class="nav02 menu__multi"><a href="#">お客様管理</a>
                <ul class="menu__second-level">
                    <li><a href="user_list.html">お客様一覧</a></li>
                </ul>
            </li>
            <li class="nav07 menu__multi"><a href="#">お知らせ管理</a>
                <ul class="menu__second-level">
                    <li><a href="information_list.html">お知らせ一覧</a></li>
                    <li><a href="information_add.html">お知らせ登録</a></li>
                </ul>
            </li>
            <li class="nav06 menu__multi"><a href="#" class="active">各種設定</a>
                <ul class="menu__second-level">
                    <li><a href="banner_list.html">バナー一覧</a></li>
                    <li><a href="banner_add.html">バナー登録</a></li>
                    <li><a href="account_list.html">アカウント一覧</a></li>
                    <li><a href="account_add.html">アカウント登録</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <!--headerNav -->
</div>
