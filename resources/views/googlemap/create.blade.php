<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Page Description">
    <meta name="author" content="DELL">
    <title>Page Title</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container">
    <div class="col-md-8 col-md-offset-2">
        <div>
            <input type="text" tabindex="8" id="address" placeholder="市町村以降" name="address" >
        </div>
        <div id="map" style="height: 500px; width: 100%; margin-top: 10px;"></div>
        <div >
            <input type="text" placeholder="26.344877, 127.873479" name="latlng" id="latlng" readonly="readonly" style="cursor: not-allowed;" >
        </div>

    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script>
    function initMap() {
        var pos_lat = 26.380596;
        var pos_lgn = 127.85684300000003;
        if ($('#latlng').val() != "") {
            pos_lat = $('#latlng').val().split(" ")[0];
            pos_lgn = $('#latlng').val().split(" ")[1];
        }
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
            center: {lat: Number(pos_lat), lng: Number(pos_lgn)}
        });
        latlng = new google.maps.LatLng(Number(pos_lat), Number(pos_lgn));
        var marker = new google.maps.Marker({
            map: map,
            position: latlng,
            draggable: true
        });
        $('input[name="latlng"]').val(Number(pos_lat) + " " + Number(pos_lgn));
        google.maps.event.addListener(marker, 'dragend', function(evt){
            $('input[name="latlng"]').val(evt.latLng.lat() + " " + evt.latLng.lng());
        });
        google.maps.event.addListener(map, 'click', function(event) {
            placeMarker(event.latLng, marker);
        });
    }
    function placeMarker(location, marker) {
        marker.setPosition(location);
        $('input[name="latlng"]').val(location.lat() + " " + location.lng());
    }

    function geocodeAddress(geocoder, resultsMap) {
        var e = document.getElementById("city");
        var city = '' !== e.options[e.selectedIndex].value ? e.options[e.selectedIndex].text : '';
        var address = document.getElementById('address').value ? document.getElementById('address').value : 'Japan';
        geocoder.geocode({'address': (city + ' ' + address)}, function(results, status) {
            if (status === 'OK') {
                resultsMap.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    map: resultsMap,
                    position: results[0].geometry.location,
                    draggable: true
                });
                google.maps.event.addListener(marker, 'dragend', function(evt){
                    $('input[name="latlng"]').val(evt.latLng.lat() + " " + evt.latLng.lng());
                });
                google.maps.event.addListener(resultsMap, 'click', function(event) {
                    placeMarker(event.latLng, marker);
                });
                console.log(results[0].geometry.location.lat());
                console.log(results[0].geometry.location.lng());
                $('input[name="latlng"]').val(results[0].geometry.location.lat() + " " + results[0].geometry.location.lng());
            } else {
                alert('次の理由により、ジオコードが成功しませんでした: ' + status);
            }
        });
    }

    $('#address').on('change', function () {
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
        });
        var geocoder = new google.maps.Geocoder();
        geocodeAddress(geocoder, map);
    });
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBc_z85PWzLEimF7jVXDGeJV6JmOB5JIaA&callback=initMap&language=ja">
</script>
</body>
</html>
